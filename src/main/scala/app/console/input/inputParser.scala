package app.console.input


import app.console.input.MyImplicits._
import app.models.usage.usageMap

import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn.readLine

object MyImplicits {
  implicit class StringImprovements(inputStr: String) {


    /**
     *
     * @param usageMap
     * @return boolean - depending if given input is in valid form of given usageMap
     * @description check input via regex completed with items from usageMap
     */
    private def inSyntaxChecker (usageMap: Map[String, Map[String,String]]):Boolean = {

      var finalReg = "run()*"

      usageMap foreach{
        case(key,value) => value foreach {
          case (key,value) => {
            if (value=="counter"){
              finalReg = finalReg.patch(4,(" --"+key+" [+-][0-9]*|"),0)
            }
            if (value=="occurrence"){
              finalReg = finalReg.patch(4," --"+key+"|",0)
            }
            if (value=="label"){
              finalReg = finalReg.patch(4," --"+key+" \\S+|",0)
            }

          }
        }
      }
      if (finalReg(finalReg.length-3)=='|'){
        finalReg = finalReg.patch(finalReg.length-3,"",1)
      }

      return inputStr.matches(finalReg);
    }

    /**
     *
     * @param usageMap
     * @return map created from input
     * @description cut inputs to basics items and then create a map from it
     */
    def parseInput (usageMap: Map[String, Map[String,String]]):Map[String,String] = {


      /*-------------------  START SYNTAX CHECKER -------------------*/

      if (!inSyntaxChecker(usageMap)){
        throw new IllegalArgumentException("Incorrect input")
      }

      /*-------------------  END SYNTAX CHECKER -------------------*/


      /*----------------------  START INPUT_MAP ----------------------*/

      var arr = ArrayBuffer[String]();
      arr = inputStr.split(" ").to(ArrayBuffer);
      arr.remove(0)
      for (x <- 0 until arr.length) {

        if (x+1 == arr.length){}
        else {
          if (arr(x).slice(0,2) == arr(x+1).slice(0,2)){
            arr.insert(x+1,"true")
          }
        }
      }

      if (arr(arr.length-1).slice(0,2) == "--"){
        arr.append("true")
      }

      var inputMap:Map[String,String] = Map()

      for (x <- 0 until arr.length) {
        if (x % 2 == 0){
          val theKey = arr(x).slice(2,arr(x).length);
          if (inputMap contains theKey){
            val countResult = inputMap(theKey).toInt + arr(x+1).toInt
            inputMap += ( theKey -> countResult.toString);
          }
          else{
            inputMap += ( theKey -> arr(x+1));
          }

        }

      }

      /*----------------------  END INPUT_MAP ----------------------*/

      return inputMap
    }
  }

}

