package app.console

import app.console.input.MyImplicits.StringImprovements
import app.models.image.{ImageFilter, Img, ImgRandom}
import app.models.usage.usageMap

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import scala.io.StdIn.readLine
import scala.sys.exit

object Main {
  def main(args: Array[String]): Unit = {


    var some = new usageMap;
    some.addRecord("filters", "rotate", "counter")
    some.addRecord("filters", "invert", "occurrence")
    some.addRecord("filters", "brightness", "counter")
    some.addRecord("image", "image", "label")
    some.addRecord("image", "output-file", "label")
    some.addRecord("image", "output-console", "occurrence")
    some.addRecord("image", "image-random", "occurrence")


    var in_in = readLine()

    var mapik = in_in.parseInput(some.getMap())


    var img: BufferedImage = null
    var loadStr: String = null
    try {
      loadStr = mapik("image").slice(1, mapik("image").length - 1);
    } catch {
      case e: NoSuchElementException => println("Exception catched")
    }

    if (loadStr == null) {
      if (mapik contains "image-random") {
        img = new ImgRandom().getRandomImg(750, 500)
      }
      else {
        throw new IllegalArgumentException("Missing image argument :(")
      }
    }
    else {
      if (mapik contains "image-random"){
        throw new IllegalArgumentException("image file and random image cant be together :(")
      }
      img = ImageIO.read(new File(loadStr))
    }

    var theImg = new Img(img)

    var imgFilter = new ImageFilter(theImg)
    var afterFilImg = imgFilter.useFilters(some.getMap(), mapik)

    try {
      var outputDes = mapik("output-file").slice(1, mapik("output-file").length - 1);
      theImg.svImg(afterFilImg, outputDes)
    } catch {
      case e: NoSuchElementException =>
        if (mapik contains "output-console") {
          theImg.svImg(afterFilImg, "output-console")
        }
        else {
          throw new IllegalArgumentException("no output location error")
        }
    }


  }
}
