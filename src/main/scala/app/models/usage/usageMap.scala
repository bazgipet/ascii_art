package app.models.usage

class usageMap {
  private var usageMap: Map[String, Map[String, String]] = Map();
  private var uMap: Map[String, String] = Map()

  /**
   *
   * @param group
   * @param name
   * @param val_type
   * @description create group or if it exists just add new record with value type to it
   */
  def addRecord(group:String, name:String, val_type:String) {

    if (val_type != "counter" && val_type != "occurrence" && val_type != "label"){
      throw new IllegalArgumentException()
    }

    uMap += (name -> val_type)
    if (usageMap contains group){
      var tempMap = usageMap (group)
      tempMap += (name->val_type)
      usageMap += (group ->tempMap)
    }
    else{
      usageMap += (group -> uMap)
    }

    uMap foreach {
      case (key, value) => uMap = uMap - key
    }

  }

  /**
   *
   * @return usageMap
   */
  def getMap (): Map[String, Map[String, String]] ={
    usageMap
  }
}

