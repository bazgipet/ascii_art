package app.models.image

import java.awt.geom.AffineTransform
import java.awt.image.{AffineTransformOp, BufferedImage}
import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn.readLine


class ImageFilter(elderImg:Img) {

  val w = elderImg.w;
  val h = elderImg.h;
  val img = elderImg.img
  var colorList = elderImg.asciiConversion(img)
  var chars_print = elderImg.colorConversion(colorList)


  /**
   *
   * @param degree
   * @return rotated image (BufferedImage) by given degree
   */
  def rotate(degree: Int): BufferedImage = {


    val rads = Math.toRadians(degree)
    val sin = Math.abs(Math.sin(rads))
    val cos = Math.abs(Math.cos(rads))
    val w = Math.floor(img.getWidth * cos + img.getHeight * sin).toInt
    val h = Math.floor(img.getHeight * cos + img.getWidth * sin).toInt
    val rotatedImage = new BufferedImage(w, h, img.getType)
    val at = new AffineTransform
    at.translate(w / 2, h / 2)
    at.rotate(rads, 0, 0)
    at.translate(-img.getWidth / 2, -img.getHeight / 2)
    val rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR)
    rotateOp.filter(img, rotatedImage)


    var temp = Math.sqrt(Math.pow(elderImg.width,2)+Math.pow(elderImg.height,2))
    elderImg.height = temp.toInt
    elderImg.width = temp.toInt


    rotatedImage

  }

  /**
   *
   * @return inverted image
   * @description going through colorList pixel by pixel and inverting its value
   */
  def invert(): BufferedImage = {
    var invertedList = new ArrayBuffer[ArrayBuffer[Pixel]]();

    for (p <- 0 until colorList.size) {
      var iList = ArrayBuffer[Pixel]();
      for (n <- 0 until colorList(p).size){
        val temp_color = 255-colorList(p)(n).getGrey_c()
        iList.append(new Pixel(temp_color, temp_color,temp_color));

      }

      invertedList.append(iList);

    }
    colorList = invertedList
    chars_print = elderImg.colorConversion(colorList)
    elderImg.crImg(chars_print)
  }

  /**
   *
   * @param bright
   * @return image with changed pixels depending on given brightness
   * @description going through colorList pixel by pixel and incrementing/decrementing its value till 0 or 255
   */
  def brightness(bright: Int): BufferedImage = {
    if (bright == 0){
      return elderImg.crImg(chars_print);
    }

    var brightList = new ArrayBuffer[ArrayBuffer[Pixel]]();

    for (p <- 0 until colorList.size) {
      var bList = ArrayBuffer[Pixel]();
      for (n <- 0 until colorList(p).size){
        var cPixel = colorList(p)(n).getGrey_c();

        if ( (cPixel + bright) >= 0 && (cPixel + bright) <= 255){
          bList.append(new Pixel ((cPixel + bright),(cPixel + bright),(cPixel + bright)));
        }
        else if ( (cPixel + bright) < 0){
          bList.append(new Pixel (0,0,0));
        }
        else {
          bList.append(new Pixel (255,255,255));
        }

      }

      brightList.append(bList);

    }
    colorList = brightList
    chars_print = elderImg.colorConversion(colorList)
    elderImg.crImg(chars_print)
  }

  /**
   *
   * @param usageMap
   * @param inputMap
   * @return image after applied of all filters in inputMap
   */
  def useFilters (usageMap:Map[String, Map[String, String]], inputMap:Map[String,String]): BufferedImage ={
    usageMap("filters") foreach{
      case (key,value) => {
        if (inputMap contains key){
          key match {
            case "rotate" => {
              var temp_img = rotate(inputMap(key).toInt)
              colorList = elderImg.asciiConversion(temp_img)
              chars_print = elderImg.colorConversion(colorList)
            }
            case "invert" => {
              invert()
            }
            case "brightness" => {
              brightness(inputMap(key).toInt)
            }
          }
        }
      }
    }
    elderImg.crImg(chars_print)
  }


}



