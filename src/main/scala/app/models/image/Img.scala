package app.models.image

import exporters.text.{ConsoleOutputExporter, ImageOutputExporter, TxtOutputExporter}

import java.awt.image.BufferedImage
import java.awt.{Color, Font}
import java.io.{File, FileWriter, IOException}
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer

class Img(img_in:BufferedImage) {

  val img = img_in;
  if (img == null){
    throw new IllegalArgumentException()
  }
  val w = img.getWidth
  val h = img.getHeight

  if (w <= 0 || h <= 0){
    throw new IllegalArgumentException()
  }

  var width = w * 3; //was * 9
  var height = h * 3; // was * 9


  /**
   *
   * @param colorList
   * @return array of array consist string - ascii characters
   */
  def colorConversion(colorList: ArrayBuffer[ArrayBuffer[Pixel]]): ArrayBuffer[ArrayBuffer[String]] = {

    var asciiList = new ArrayBuffer[ArrayBuffer[String]]();

    for (p <- 0 until colorList.size) {
      var alist = ArrayBuffer[String]();
      for (n <- 0 until colorList(p).size){

        var greyPixel = colorList(p)(n).getGrey_c()

        if (greyPixel > 233) {
          //chars_print += " "
          alist.append(" ");
        }
        else if (greyPixel > 133) {
          //chars_print += "."
          alist.append(".");
        }
        else if (greyPixel > 83) {
          //chars_print += ":"
          alist.append(":");
        }
        else if (greyPixel > 40) {
          //chars_print += "x"
          alist.append("x");
        }
        else {
          //chars_print += "$"
          alist.append("$");
        }

      }
      asciiList.append(alist);
    }

    return asciiList;

  }

  /**
   *
   * @param img
   * @return array of array consist of pixels
   */
  def asciiConversion(img: BufferedImage): ArrayBuffer[ArrayBuffer[Pixel]] = {

    val w = img.getWidth
    val h = img.getHeight
    var colorList = ArrayBuffer[ArrayBuffer[Pixel]]();

    for (n <- 0 until h) {
      var cList = ArrayBuffer[Pixel]();

      for (m <- 0 until w) {

        val pixel = new Pixel(img.getRGB(m, n))
        cList.append(pixel);

      }

      colorList.append(cList);

    }

    return colorList;
  }

  /**
   *
   * @param chars_print
   * @return image created with ascii characters
   */
  def crImg (chars_print: ArrayBuffer[ArrayBuffer[String]]):BufferedImage = {
    val bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

    val g2d = bufferedImage.createGraphics
    g2d.setColor(Color.white)
    val mono_font = new Font("Monospaced", Font.PLAIN, 5)  //was 15
    val fm = g2d.getFontMetrics(mono_font)
    g2d.setFont(mono_font);
    g2d.fillRect(0, 0, width, height)
    g2d.setColor(Color.BLACK)


    for (p <- 0 until chars_print.size) {
      val str = chars_print(p).mkString("")
      g2d.drawString(str, 0, (3 * p)) // was * 9
    }

    // Disposes of this graphics context and releases any system resources that it is using.
    g2d.dispose()

    return bufferedImage

  }

  /**
   *
   * @param img
   * @param outputDes
   * @return boolean if it saved img successfully
   */
  def svImg (img:BufferedImage, outputDes:String): Boolean ={

    if (outputDes == null){
      return false
    }

    // Print to console
    if (outputDes == "output-console"){
      val conExpo = new ConsoleOutputExporter()
      conExpo.export(img)
      return true
    }

    // Save as PNG
    if (outputDes.matches(".*\\.png$")){
      val imgExpo = new ImageOutputExporter(new File (outputDes))
      imgExpo.export(img)
      return true
    }

    // Save as TXT
    if (outputDes.matches(".*\\.txt$")){
      val txtExpo = new TxtOutputExporter(outputDes)
      txtExpo.export(img)
      return true
    }

    false

  }

}

