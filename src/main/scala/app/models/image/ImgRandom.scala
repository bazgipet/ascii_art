package app.models.image

import java.awt.image.BufferedImage

class ImgRandom (){

  /**
   *
   * @param width
   * @param height
   * @return random generated image with given width and height
   */
  def getRandomImg(width: Int, height: Int): BufferedImage = {

    if (width <= 0 || height <= 0){
      throw new IllegalArgumentException()
    }

    // Create buffered image object
    val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

    // create random values pixel by pixel
    for (y <- 0 until height) {
      for (x <- 0 until width) { // generating values less than 256
        val a = (Math.random * 256).toInt
        val r = (Math.random * 256).toInt
        val g = (Math.random * 256).toInt
        val b = (Math.random * 256).toInt
        //pixel
        val p = (a << 24) | (r << 16) | (g << 8) | b
        img.setRGB(x, y, p)
      }
    }

    return img
  }

}




