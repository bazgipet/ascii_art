package app.models.image

class Pixel(color: Int) {

  /*if (color < 0){
    throw new IllegalArgumentException()
  }*/

  private var red: Int = ((color >> 16) & 0xff);
  private var green: Int = ((color >> 8) & 0xff);
  private var blue: Int = (color & 0xff);

  def this (r:Int, g:Int, b:Int){
    this(((r << 16) + (g << 8) + b));
    if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255){
      throw new IllegalArgumentException()
    }
  }

  def getRed (): Int ={
    red
  }
  def getGreen (): Int ={
    green
  }
  def getBlue (): Int ={
    blue
  }

  /**
   *
   * @return grey color pixel
   */
  def getGrey_c (): Int ={
    ((red + green + blue)/3)
  }


}


