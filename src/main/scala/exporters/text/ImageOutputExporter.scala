package exporters.text

import java.awt.image.BufferedImage
import java.io.{File, FileOutputStream, IOException}
import javax.imageio.ImageIO

class ImageOutputExporter(file: File) extends TextExporter()
{

  protected def exportToImg(item: BufferedImage): Unit ={
    if (file.exists()){
      throw new IllegalArgumentException()
    }
    try {
      ImageIO.write(item, "png", file)
    } catch {
      case e: IOException => println("Had an IOException")
    }
  }


  override def export(item: BufferedImage): Unit = exportToImg(item)
}

/*
object trying2 {
  def main(args: Array[String]): Unit = {
    val theImg = ImageIO.read(new File("ter2.jpg"))
    val imgExpo = new ImageOutputExporter(new File ("result_try.png"))
    imgExpo.export(theImg)
  }
}
*/
