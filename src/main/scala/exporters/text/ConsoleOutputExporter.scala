package exporters.text

import app.models.image.Img

import java.awt.image.BufferedImage
import java.io.{File, IOException}
import javax.imageio.ImageIO

class ConsoleOutputExporter extends TextExporter()
{
  protected def exportToConsole(item: BufferedImage): Unit ={
    val theImg = new Img(item)
    val colorList = theImg.asciiConversion(item);
    val chars_print = theImg.colorConversion(colorList);

    for (p <- 0 until chars_print.size) {
      println(chars_print(p).mkString(""))
    }

  }


  override def export(item: BufferedImage): Unit = exportToConsole(item)
}


object trying222 {
  def main(args: Array[String]): Unit = {
    val theImg = ImageIO.read(new File("ter2.jpg"))
    val conExpo = new ConsoleOutputExporter()
    conExpo.export(theImg)
  }
}
