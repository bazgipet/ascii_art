package exporters.text

import app.models.image.Img

import java.awt.image.BufferedImage
import java.io.{File, FileWriter, IOException}
import javax.imageio.ImageIO

class TxtOutputExporter(outputDes: String) extends TextExporter()
{

  protected def exportToConsole(item: BufferedImage): Unit ={

    if (new File(outputDes).exists()){
      throw new IllegalArgumentException()
    }

    val theImg = new Img(item)
    val colorList = theImg.asciiConversion(item);
    val chars_print = theImg.colorConversion(colorList);

      try {
        val myObj = new File(outputDes)
        if (myObj.createNewFile) {
          try {
            val myWriter = new FileWriter(outputDes)

            for (p <- 0 until chars_print.size) {
              println(p)
              myWriter.write(chars_print(p).mkString(""))
            }
            myWriter.close()

            System.out.println("Successfully wrote to the file.")
          } catch {
            case e: IOException =>
              System.out.println("An error occurred.")
              e.printStackTrace()
          }
        }
        else {
          System.out.println("File already exists.")
        }
      } catch {
        case e: IOException =>
          System.out.println("An error occurred.")
          e.printStackTrace()
      }

    }

  override def export(item: BufferedImage): Unit = exportToConsole(item)
}

/*
object trying22 {
  def main(args: Array[String]): Unit = {
    val theImg = ImageIO.read(new File("ter2.jpg"))
    val txtExpo = new TxtOutputExporter("result_try.txt")
    txtExpo.export(theImg)
  }
}
 */