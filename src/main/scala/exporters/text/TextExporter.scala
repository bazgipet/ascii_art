package exporters.text

import exporters.Exporter

import java.awt.image.BufferedImage

trait TextExporter extends Exporter[BufferedImage]
{

}
