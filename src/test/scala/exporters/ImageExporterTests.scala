package exporters

import app.models.image.{ImageFilter, Img, ImgRandom, Pixel}
import exporters.text.ImageOutputExporter
import org.scalatest.FunSuite

import javax.imageio.ImageIO
import java.io.File
import java.io.IOException
import java.nio.file.{Files, Paths}

class ImageExporterTests extends FunSuite {

  test("file_successfully_created"){

    val dest = "result_test.png"

    try {
      val randomImg = new ImgRandom().getRandomImg(500,325)
      val imgExpo = new ImageOutputExporter(new File (dest))
      imgExpo.export(randomImg)
      val temp = File.createTempFile("result_test", ".png")
      if (!temp.exists){
        fail()
      }
      Files.deleteIfExists(Paths.get(dest));
    }catch {
      case e: IOException =>
        e.printStackTrace()
    }

  }

  test("file_already_exists"){
    val dest = "result_test.png"

    try {
      val randomImg = new ImgRandom ().getRandomImg(500,325)
      if (!new File (dest).exists()){
        new File (dest).createNewFile()
      }
      val imgExpo = new ImageOutputExporter(new File (dest))
      imgExpo.export(randomImg)
      fail("has exception occurred")
    }catch {
      case e: IllegalArgumentException =>
        println("passed")
    }
    finally {
      Files.deleteIfExists(Paths.get(dest));
    }
  }

}