package exporters

import app.models.image.{ImageFilter, Img, ImgRandom, Pixel}
import exporters.text.{ImageOutputExporter, TxtOutputExporter}
import org.scalatest.FunSuite

import javax.imageio.ImageIO
import java.io.File
import java.io.IOException
import java.nio.file.{Files, Paths}

class TxtOutputExporterTests extends FunSuite {

  test("file_successfully_created"){

    val dest = "result_test.txt"

    try {

      val randomImg = new ImgRandom().getRandomImg(500,325)

      val txtExpo = new TxtOutputExporter(dest)
      txtExpo.export(randomImg)

      val temp = File.createTempFile("result_test", ".txt")
      if (!temp.exists){
        fail()
      }
      Files.deleteIfExists(Paths.get(dest));
    }catch {
      case e: IOException =>
        e.printStackTrace()
    }

  }

  test("file_already_exists"){
    val dest = "result_test.txt"

    try {
      val randomImg = new Img (new ImgRandom().getRandomImg(500,325))
      if (!new File (dest).exists()){
        new File (dest).createNewFile()
      }

      val txtExpo = new TxtOutputExporter(dest)
      txtExpo.export(randomImg.img)
      fail("has exception occurred")
    }catch {
      case e: IllegalArgumentException =>
        println("passed")
    }
    finally {
      Files.deleteIfExists(Paths.get(dest));
    }
  }

}