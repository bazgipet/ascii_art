package input

import app.console.input.MyImplicits.StringImprovements
import app.models.image.{ImageFilter, Img, ImgRandom, Pixel}
import app.models.usage.usageMap
import org.scalatest.FunSuite

import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
import java.io.File
import java.io.IOException
import java.nio.file.{Files, Paths}

class InputTests extends FunSuite {

  test ("missing_run_in_command"){
    var some = new usageMap;
    some.addRecord("filters", "rotate", "counter")
    some.addRecord("filters", "invert", "occurrence")
    some.addRecord("filters", "brightness", "counter")
    some.addRecord("image", "image", "label")
    some.addRecord("image", "output-file", "label")
    some.addRecord("image", "output-console", "occurrence")


    val in_str = "--image \"ter2.jpg\" --output-file \"result.png\""
    try{
      in_str.parseInput(some.getMap())
    }
    catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }
  test ("missing_--_in_command"){
    var some = new usageMap;
    some.addRecord("filters", "rotate", "counter")
    some.addRecord("filters", "invert", "occurrence")
    some.addRecord("filters", "brightness", "counter")
    some.addRecord("image", "image", "label")
    some.addRecord("image", "output-file", "label")
    some.addRecord("image", "output-console", "occurrence")


    val in_str = "run --image \"ter2.jpg\" output-file \"result.png\""
    try{
      in_str.parseInput(some.getMap())
    }
    catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }
  test ("missing_+_in_filter"){
    var some = new usageMap;
    some.addRecord("filters", "rotate", "counter")
    some.addRecord("filters", "invert", "occurrence")
    some.addRecord("filters", "brightness", "counter")
    some.addRecord("image", "image", "label")
    some.addRecord("image", "output-file", "label")
    some.addRecord("image", "output-console", "occurrence")


    val in_str = "run --image \"ter2.jpg\" --brightness 90 --output-file \"result.png\""
    try{
      in_str.parseInput(some.getMap())
    }
    catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }
  test ("missing_-_in_command"){
    var some = new usageMap;
    some.addRecord("filters", "rotate", "counter")
    some.addRecord("filters", "invert", "occurrence")
    some.addRecord("filters", "brightness", "counter")
    some.addRecord("image", "image", "label")
    some.addRecord("image", "output-file", "label")
    some.addRecord("image", "output-console", "occurrence")


    val in_str = "run --image \"ter2.jpg\" --rotate 45 --output-file \"result.png\""
    try{
      in_str.parseInput(some.getMap())
    }
    catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }
  test ("using_dont_known_filter"){
    var some = new usageMap;
    some.addRecord("filters", "rotate", "counter")
    some.addRecord("filters", "invert", "occurrence")
    some.addRecord("filters", "brightness", "counter")
    some.addRecord("image", "image", "label")
    some.addRecord("image", "output-file", "label")
    some.addRecord("image", "output-console", "occurrence")


    val in_str = "run --image \"ter2.jpg\" --flip --output-file \"result.png\""
    try{
      in_str.parseInput(some.getMap())
    }
    catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }


}