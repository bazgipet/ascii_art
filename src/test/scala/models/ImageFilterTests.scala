package models

import app.models.image.{ImageFilter, Img, Pixel}
import org.scalatest.FunSuite


import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer

class ImageFilterTests extends FunSuite {

  test("rotate_positive_value"){

    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val imgBeforeRotation = imgFilter.rotate(0)
    val imgAfterRotation = imgFilter.rotate(90)

    assert (imgBeforeRotation.getRGB(0, 0) != imgAfterRotation.getRGB(0, 0))

  }

  test("rotate_negative_value"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val imgBeforeRotation = imgFilter.rotate(0)
    val imgAfterRotation = imgFilter.rotate(-45)

    assert (imgBeforeRotation.getRGB(0, 0) != imgAfterRotation.getRGB(0, 0))
  }

  test("rotate_360_value"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val imgBeforeRotation = imgFilter.rotate(0)
    val imgAfterRotation = imgFilter.rotate(360)

    assert (imgBeforeRotation.getRGB(0, 0) == imgAfterRotation.getRGB(0, 0))
  }

  test("invert_test"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val random_height = theImg.img.getHeight/10
    val random_width = theImg.img.getWidth/10

    val cl_before = imgFilter.colorList(random_width)(random_height).getGrey_c()
    val imgAfterInvert = imgFilter.invert()

    val cl_after = imgFilter.colorList(random_width)(random_height).getGrey_c()

    assert (255-cl_before == cl_after)


  }

  test("brightness_positive_value"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val random_height = theImg.img.getHeight/10
    val random_width = theImg.img.getWidth/10

    val cl_before = imgFilter.colorList(random_width)(random_height).getGrey_c()

    val imgAfterBrightness = imgFilter.brightness(90)

    val cl_after = imgFilter.colorList(random_width)(random_height).getGrey_c()

    assert (cl_before+90 == cl_after || cl_before+90 == 255)


  }

  test("brightness_negative_value"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val random_height = theImg.img.getHeight/10
    val random_width = theImg.img.getWidth/10

    val cl_before = imgFilter.colorList(random_width)(random_height).getGrey_c()

    val imgAfterBrightness = imgFilter.brightness(-90)

    val cl_after = imgFilter.colorList(random_width)(random_height).getGrey_c()

    assert (cl_before-90 == cl_after || cl_before-90 <= 0)


  }

  test("brightness_all_white"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val random_height = theImg.img.getHeight/10
    val random_width = theImg.img.getWidth/10

    val imgAfterBrightness = imgFilter.brightness(255)

    val cl_after = imgFilter.colorList(random_width)(random_height).getGrey_c()

    assert (cl_after == 255)


  }

  test("brightness_all_black"){
    val theImg = new Img (ImageIO.read(new File("ter2.jpg")))
    val imgFilter = new ImageFilter(theImg)
    val random_height = theImg.img.getHeight/10
    val random_width = theImg.img.getWidth/10

    val imgAfterBrightness = imgFilter.brightness(-255)

    val cl_after = imgFilter.colorList(random_width)(random_height).getGrey_c()

    assert (cl_after == 0)


  }

}