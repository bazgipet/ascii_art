package models

import app.models.image.Pixel
import org.scalatest.FunSuite

class PixelTests extends FunSuite {

  test("get_red_value"){

    val thePixel = new Pixel(255,200,125);
    assert(thePixel.getRed() == 255)

  }
  test("get_green_value"){

    val thePixel = new Pixel(255,200,125);
    assert(thePixel.getGreen() == 200)

  }
  test("get_blue_value"){

    val thePixel = new Pixel(255,200,125);
    assert(thePixel.getBlue() == 125)

  }
  test("get_grey_color"){

    val thePixel = new Pixel(255,200,125);
    assert(thePixel.getGrey_c() == (255+200+125)/3)

  }

  test ("invalid_argument_negative"){
    try {
      val thePixel = new Pixel(-55,48,12);
      fail("didnt throw exception")
    } catch {
      case e: IllegalArgumentException => println("Exception catched")
    }
  }

  test ("invalid_argument_behind_255"){
    try {
      val thePixel = new Pixel(55,256,12);
      fail("didnt throw exception")
    } catch {
      case e: IllegalArgumentException => println("Exception catched")
    }
  }

}