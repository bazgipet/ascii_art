package models

import app.models.image.{ImageFilter, Img, ImgRandom, Pixel}
import org.scalatest.FunSuite

import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer

class ImgRandomTests extends FunSuite {

  test("check_size_of_ranImg"){

    val randomImg = new ImgRandom ().getRandomImg(500,325)
    assert (randomImg.getWidth() == 500 && randomImg.getHeight() == 325)

  }

  test("check_invalid_value"){

    try {
      val randomImg = new ImgRandom ().getRandomImg(-50,32)
      fail("didnt throw exception")
    } catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }

}