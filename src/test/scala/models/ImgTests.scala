package models

import app.models.image.{ImageFilter, Img, ImgRandom, Pixel}
import org.scalatest.FunSuite

import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
import java.io.File
import java.io.IOException
import java.nio.file.{Files, Paths}

class ImgTests extends FunSuite {

  test("check_image_size"){

    val theImg = new Img (new ImgRandom().getRandomImg(500,300))
    assert(theImg.w == 500 && theImg.h == 300)

  }

  test ("saved_image_exists"){
    val randomImg = new ImgRandom().getRandomImg(500,300)
    val theImg = new Img (randomImg)
    theImg.svImg(randomImg, "testingImg.png")

    try {
      var temp = File.createTempFile("testingImg", ".png")
      if (!temp.exists){
        fail()
      }
    } catch {
      case e: IOException =>
        e.printStackTrace()
    }
    finally {
      Files.deleteIfExists(Paths.get("testingImg.png"));
    }

  }

  test ("invalid_img_size_and_format"){

    try {
      val theImg = new Img(ImageIO.read(new File("zero_zero.png")))
      fail("didnt throw exception")
    } catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }

}