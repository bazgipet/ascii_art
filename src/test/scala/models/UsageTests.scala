package models

import app.models.image.{ImgRandom, Pixel}
import app.models.usage.usageMap
import org.scalatest.FunSuite

class UsageTests extends FunSuite {

  test("group_exists"){

    val theMap = new usageMap;
    theMap.addRecord("filters", "rotate", "counter")

    if (!theMap.getMap().contains("filters")){
      fail()
    }

  }

  test("record_exists"){

    val theMap = new usageMap;
    theMap.addRecord("filters", "rotate", "counter")
    theMap.addRecord("filters", "invert", "occurrence")
    theMap.addRecord("filters", "brightness", "counter")
    theMap.addRecord("image", "image", "label")

    if (!theMap.getMap()("filters").contains("rotate")){
      fail()
    }

  }

  test("invalid_type_argument"){

    try {
      val theMap = new usageMap;
      theMap.addRecord("filters", "rotate", "something_not_allowed")
      fail("didnt throw exception")
    } catch {
      case e: IllegalArgumentException => println("Exception catched")
    }

  }

}